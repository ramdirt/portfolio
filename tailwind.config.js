/** @type {import('tailwindcss').Config} */
// eslint-disable-next-line no-undef
module.exports = {
	content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
	separator: "_",
	theme: {
		extend: {
			fontFamily: {
				sans: "mono",
			},
		},
		container: {
			center: true,
		},
	},
	plugins: [],
};
